import tweepy
import json
from gluon import *

access_token = "4193633293-okO1Ffk1ogWrgCoqcIbmHGN0xZehc0oaL0nd6JW" ##NOT PUBLIC
access_token_secret = "GSoIWvObhDoqKlgSiZ7G9Xyuse0oEiEz7mDRt7CW6ldKF" ##NOT PUBLIC
consumer_key = "xpbVg3aUBKi517wKDTqsOg1Lm"
consumer_secret = "cVSV9ukPuO5PbiBY7rXcVfcY9hnrtPOvoH5yKisnv15rUQCiNr"

@auth.requires_login()
def iptest():
    if (str(current.request.client) != '127.0.0.1'):
        var = "no"
    else:
        var = "yes"
    return dict(ip = str(current.request.client), var = var)


@auth.requires_login()
def testgetprofile():
    """
    TEMP TO TEST OUT API BY SEARCHING PROFILE INFO
    """
    if (str(current.request.client) != "127.0.0.1") and (str(current.request.client) != "192.210.193.226") and (str(current.request.client) != "198.46.146.165"):
        redirect(URL('default', 'index'))
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.secure = True
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)
    temp = api.get_user(id='TheDailyShow')
    return dict(temp = temp)

@auth.requires_login()
def testsearchtweets():
    """

    """
    if (str(current.request.client) != "127.0.0.1") and (str(current.request.client) != "192.210.193.226") and (str(current.request.client) != "198.46.146.165"):
        redirect(URL('default', 'index'))
    if request.args(0) is None:
        redirect(URL('default', 'index'))
    query = request.args(0)
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.secure = True
    auth.set_access_token(access_token, access_token_secret)
    api= tweepy.API()
    searched_tweets = tweepy.Cursor(api.search,
                           q=query,
                           rpp=100,
                           result_type="recent",
                           include_entities=True,
                           lang="en").items()
    return dict(searched_tweets = searched_tweets)

@auth.requires_login()
def testsearch():
    if (str(current.request.client) != "127.0.0.1") and (str(current.request.client) != "192.210.193.226") and (str(current.request.client) != "198.46.146.165"):
        redirect(URL('default', 'index'))
    if request.args(0) is None:
        redirect(URL('default', 'index'))
    query = request.args(0)
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.secure = True
    auth.set_access_token(access_token, access_token_secret)
    api= tweepy.API(auth, parser=tweepy.parsers.JSONParser())
    results = api.search(q=query, count=5)
    return dict(results = results)

@auth.requires_login()
def testsearch2():
    if (str(current.request.client) != "127.0.0.1") and (str(current.request.client) != "192.210.193.226") and (str(current.request.client) != "198.46.146.165"):
        redirect(URL('default', 'index'))
    if request.args(0) is None:
        redirect(URL('default', 'index'))
    query = "#" + request.args(0)
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.secure = True
    auth.set_access_token(access_token, access_token_secret)
    api= tweepy.API(auth, parser=tweepy.parsers.JSONParser())
    results = api.search(q=query, count=100)
    return dict(results = results, searchstring = query)

@auth.requires_login()
def gettweet():
    """
    Get single tweet for showing one post only
    """
    if (str(current.request.client) != "127.0.0.1") and (str(current.request.client) != "192.210.193.226") and (str(current.request.client) != "198.46.146.165"):
        redirect(URL('default', 'index'))
    query = request.args(0)
    if query is None:
        query = '673931235023036416';
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.secure = True
    auth.set_access_token(access_token, access_token_secret)
    api= tweepy.API(auth, parser=tweepy.parsers.JSONParser())
    tweet = api.get_status(id=query)
    return dict(tweet = tweet)
