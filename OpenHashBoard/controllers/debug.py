from gluon import *
import re

@auth.requires_login()
def displayusersettingslist():
    """
    Just some user data to check user settings
    """
    if (str(current.request.client) != "127.0.0.1") and (str(current.request.client) != "192.210.193.226") and (str(current.request.client) != "198.46.146.165"):
        redirect(URL('default', 'index'))
    userdata = db((db.user_settings.user_id == auth.user_id))
    userdatas = userdata.select()
    return dict(user = userdatas)

@auth.requires_login()
def displaysharedlist():
    """
    Just some user data to check user settings
    """
    if (str(current.request.client) != "127.0.0.1") and (str(current.request.client) != "192.210.193.226") and (str(current.request.client) != "198.46.146.165"):
        redirect(URL('default', 'index'))
    userdata = db((db.shared_searches.user_id == auth.user_id))
    userdatas = userdata.select()
    return dict(user = userdatas)



@auth.requires_login()
def removefirst():
    """
    TEMP TO CLEAR OUT OLD user settings. Useful if there are duplicate board settings
    """
    if (str(current.request.client) != "127.0.0.1") and (str(current.request.client) != "192.210.193.226") and (str(current.request.client) != "198.46.146.165"):
        redirect(URL('default', 'index'))
    userdata = db((db.user_settings.user_id == auth.user_id))
    userdatas = userdata.select().first().id
    del db.user_settings[userdatas]
    return dict(user = "ok")

@auth.requires_login()
def fillboardbackend():
    """
    Recursive technique of appending searches to the list for debug/design purposes
    """
    if (str(current.request.client) != "127.0.0.1") and (str(current.request.client) != "192.210.193.226") and (str(current.request.client) != "198.46.146.165"):
        redirect(URL('default', 'index'))
    temp = request.args(0)
    fav = request.args(1)
    if temp is not None:
        if fav is not None:
            db.searches.insert(
                search_string = temp,
                favorite = True,
            )
        else:
           db.searches.insert(
                search_string = temp,
            )


@auth.requires_login()
def checkutcoffsetworks():
    if (str(current.request.client) != "127.0.0.1") and (str(current.request.client) != "192.210.193.226") and (str(current.request.client) != "198.46.146.165"):
        redirect(URL('default', 'index'))
    userdata = db((db.user_settings.user_id == auth.user_id)).select().last()
    if userdata.utcoffset is not None:
        dataoff = str(userdata.utcoffset)
        temp = int(re.search(r'\d+', dataoff).group())
        if '-' in dataoff:
            offset = 0 - temp
        else:
            offset = temp
    if userdata.utcoffset is None:
        offset=0
    return dict(offset = offset)