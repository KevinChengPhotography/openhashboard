# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################
import re
from datetime import date

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    countmain = 2000        # Number of hashtag to display in the background
    countpast = 10          # Number of past hashtag to show
    countfavorite = 10      # Number of favorite Hashtags
    countshared = 10        # Number of shared searches
    pastsearches = db(db.searches).select()
    sharedsearches = db(db.shared_searches.to_user_id == auth.user_id).select()
    return dict(pastsearches = pastsearches, countmain = countmain,
                countpast = countpast, countfavorite = countfavorite,
                sharedsearches = sharedsearches, countshared = countshared)

@auth.requires_login()
@auth.requires_signature()
def boardsettings():
    """
    Allow the user to change display settings of their board.
    Background_image: allow the user to change their background image
    Refresh Interval: Change how many seconds to wait before requesting new posts
    Post Width: How long is one post width, in px
    Show Header: enable/disable showing the header
    Twitter Enable: To show twitter posts? Just disregard this for now.
    """
    pastsearches = db(db.searches).select()
    db.user_settings.user_id.readable = db.user_settings.user_id.writable = False
    db.user_settings.twitter_enable.readable = db.user_settings.twitter_enable.writable = False
    form = SQLFORM(db.user_settings)
    userdata = db((db.user_settings.user_id == auth.user_id)).select().last()
    if userdata is not None:
        form.vars.user_id = auth.user_id
        form.vars.background_image = userdata.background_image
        # Some string manipulation to get the values from the database correctly
        # First, print to function, then do a python version of atoi
        refreshstr = str(userdata.refresh_interval)
        refreshint = int(re.search(r'\d+', refreshstr).group())
        form.vars.refresh_interval = refreshint
        form.vars.post_width = userdata.post_width
        if userdata.utcoffset is not None:
            utctempoff = str(userdata.utcoffset)
            utcinttemp = int(re.search(r'\d+', utctempoff).group())
            if '-' in utctempoff:
                offset = 0 - utcinttemp
            else:
                offset = utcinttemp
            form.vars.utcoffset = offset
        form.vars.show_header = userdata.show_header
        form.vars.twitter_enable = userdata.twitter_enable
        form.vars.logo_name = userdata.logo_name
        form.vars.logo_image = userdata.logo_image
        form.vars.show_logo_on_board = userdata.show_logo_on_board
    if form.process().accepted:
        if userdata is not None:
            del db.user_settings[userdata.id]
        redirect(URL('default', 'index'))
    return dict(form=form, pastsearches = pastsearches)

def createsettingsregister(fields, id):
    """
    When the user registers with the website, create a settings for the user with the default
    settins we prescribe
    """
    db.user_settings.insert(
        user_id = id,
        background_image = "",
        refresh_interval = 0,
        post_width = 200,
        utcoffset = 0,
        show_header = True,
        twitter_enable = True,
        logo_name = "OpenHashBoard",
        logo_image = "",
        show_logo_on_board = True
    )

def credits():
    year = date.today().year
    return dict(year = year)

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    pastsearches = db(db.searches).select()
    db.auth_user._after_insert.append(createsettingsregister) # check this for me later CHECK
    return dict(form=auth(), pastsearches = pastsearches)


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


