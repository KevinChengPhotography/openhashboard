import tweepy
import datetime
from datetime import timedelta
import re

access_token = "4193633293-okO1Ffk1ogWrgCoqcIbmHGN0xZehc0oaL0nd6JW" ##NOT PUBLIC
access_token_secret = "GSoIWvObhDoqKlgSiZ7G9Xyuse0oEiEz7mDRt7CW6ldKF" ##NOT PUBLIC
consumer_key = "xpbVg3aUBKi517wKDTqsOg1Lm"
consumer_secret = "cVSV9ukPuO5PbiBY7rXcVfcY9hnrtPOvoH5yKisnv15rUQCiNr"


@auth.requires_login()
def savehash():
    """
    STAR/FAVORITE THE HASH SEARCHES FOR LATER REFRENCE. WHEN THE USER
    IS IN THE SEARCH FIELDS, HE WILL SEE A LIST OF FAVORITES
    """
    searchvalue = request.vars.searchstring
    varmode = request.vars.mode
    if varmode is 's':
        pastsearches = db((db.searches.search_string == searchvalue) & (db.searches.user_id == auth.user_id) & (db.searches.favorite == False)).select().last()
        if pastsearches is not None:
            db.searches[pastsearches.id] = dict(favorite = True)
    else:
        pastsearches = db((db.searches.search_string == searchvalue) & (db.searches.user_id == auth.user_id) & (db.searches.favorite == True)).select().last()
        if pastsearches is not None:
            db.searches[pastsearches.id] = dict(favorite = False)
    return 'ok'

@auth.requires_login()
def index():
    """
    LAUNCH THE SEARCHES FOR A CERTAIN HASH.
    IN PROGRESS TO INTEGRATE TWITTER API FUNCTIONS TO IT
    """
    searchtag = request.args(0)
    if searchtag is None:
        redirect(URL('default', 'index'))
    searchdatabase = db((db.searches.search_string == searchtag) &
                        (db.searches.user_id == auth.user_id)).select().last()
    if searchdatabase is None:
        db.searches.insert(
            search_string = searchtag,
        )
        searchdatabase = db((db.searches.search_string == searchtag) &
                            (db.searches.user_id == auth.user_id)).select().last()
    userdata = db((db.user_settings.user_id == auth.user_id)).select().last()
    query = '%23' + request.args(0)
    authtwitter = tweepy.OAuthHandler(consumer_key, consumer_secret)
    authtwitter.secure = True
    authtwitter.set_access_token(access_token, access_token_secret)
    api= tweepy.API(authtwitter, parser=tweepy.parsers.JSONParser())
    refreshstr = str(userdata.refresh_interval)
    refreshint = int(re.search(r'\d+', refreshstr).group())
    results = api.search(q=query, count=100)
    return dict(searchtag = searchtag, searchdatabase = searchdatabase, results = results, userdata = userdata,
                getRefreshinterval = getRefreshinterval, convertdatetime = convertdatetime, refreshint = refreshint)

@auth.requires_login()
def share():
    searchstring = str(request.args(0))
    db.shared_searches.user_id.readable = db.shared_searches.user_id.writable = False
    db.shared_searches.search_string.default = searchstring
    db.shared_searches.search_string.readable = db.shared_searches.search_string.writable = False
    form = SQLFORM(db.shared_searches)
    if form.process().accepted:
        redirect(URL('search', 'closepopup'))
    return dict(form=form)

def closepopup():
    return dict()

@auth.requires_login()
def tweet():
    tweetid = request.args(0)
    query = tweetid
    if tweetid is None:
        redirect(URL('default', 'index'))
    userdata = db((db.user_settings.user_id == auth.user_id)).select().last()
    authtwitter = tweepy.OAuthHandler(consumer_key, consumer_secret)
    authtwitter.secure = True
    authtwitter.set_access_token(access_token, access_token_secret)
    api= tweepy.API(authtwitter, parser=tweepy.parsers.JSONParser())
    results = api.get_status(id=query)
    return dict(results = results, userdata = userdata, convertdatetime = convertdatetime)

def getRefreshinterval():
    userdata = db((db.user_settings.user_id == auth.user_id)).select().last()
    refresh_interval = userdata.refresh_interval
    return refresh_interval


def convertdatetime(datestring):
    """
    Get the datetime input and converts to a simple, more elegant date
    for easier reading, removing years and seconds and just getting
    the date and hour/minute
    """
    date = datetime.datetime.strptime(datestring, "%a %b %d %X +0000 %Y")
    userdata = db((db.user_settings.user_id == auth.user_id)).select().last()
    if userdata.utcoffset is not None:
        utctempoff = str(userdata.utcoffset)
        utcinttemp = int(re.search(r'\d+', utctempoff).group())
        if '-' in utctempoff:
            newtime = date - timedelta(hours=utcinttemp)
        else:
            newtime = date + timedelta(hours=utcinttemp)
    else:
        newtime = date
    return newtime.strftime('%b %d, %Y %I:%M%p')